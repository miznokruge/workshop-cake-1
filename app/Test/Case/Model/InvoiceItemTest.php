<?php
App::uses('InvoiceItem', 'Model');

/**
 * InvoiceItem Test Case
 *
 */
class InvoiceItemTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.invoice_item',
		'app.product',
		'app.sale_item',
		'app.sale',
		'app.clinic',
		'app.customer',
		'app.invoice'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->InvoiceItem = ClassRegistry::init('InvoiceItem');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->InvoiceItem);

		parent::tearDown();
	}

}
